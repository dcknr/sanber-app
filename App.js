import { StatusBar } from 'expo-status-bar';
import React from 'react';

import { StyleSheet, Text, View } from 'react-native';
import YouTubeUI from './Tugas/Tugas12/App';
import LoginScreen from './Tugas/Tugas13/components/LoginScreen';
import RegisterScreen from './Tugas/Tugas13/components/RegisterScreen';
import AboutScreen from './Tugas/Tugas13/components/AboutScreen';
import SkillScreen from './Tugas/Tugas14/components/SkillScreen';
import Notes from './Tugas/Tugas14/App';
// import Index from './index';
// import Index from './Tugas/Tugas15/index';
// import Index from './Tugas/TugasNavigation/index';
import Index from './Quiz3/index';

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
    <View style={styles.container}>
      {/* <StatusBar style="auto" /> */}
      {/* <YouTubeUI/> */}
      {/* <LoginScreen/> */}
      {/* <RegisterScreen/> */}
      {/* <AboutScreen/> */}
      {/* <SkillScreen/> */}
      {/* <Notes/> */}
      <Index/>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
