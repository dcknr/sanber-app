// import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, Image, View, Dimensions } from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

const {width : WIDTH} = Dimensions.get('window')

export default class AboutScreen extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Text style={{fontSize:36, marginTop: 30 }}>Tentang Saya</Text>
                    <Image
                        style={styles.person}
                        source = {require(".././images/person.png")}
                    />
                    <Text style={styles.textPerson}>Anri Dicky Septiawan</Text>
                    <Text style={styles.textDeveloper}>React Native Developer</Text>
                </View>
                
                <View style={styles.portofolio}>
                    <Text style={styles.textTitle}>Portofolio</Text>
                    <View style={styles.borderHitam}></View>
                    
                    <View style={styles.rawMargin}>
                        <View style={styles.columnMargin}>
                            <Icon style={{fontSize:46, color:"#3EC6FF",marginBottom: 5, }} name="gitlab"></Icon>
                            <Text style={styles.textIcon}>@dcknr</Text>
                        </View>
                        <View style={styles.columnMargin}>
                            <Icon style={{fontSize:46, color:"#3EC6FF",marginBottom: 5, }} name="github"></Icon>
                            <Text style={styles.textIcon}>@dcknr</Text>
                        </View>
                    </View>
                    
                </View>

                <View style={styles.hubungiSaya}>
                    <Text style={styles.textTitle}>Hubungi Saya</Text>
                    <View style={styles.borderHitam}></View>
                    
                    <View style={styles.columnMargin}>
                        <View style={styles.rawMargin}>
                            <Icon style={{fontSize:46, color:"#3EC6FF", alignSelf:"center" }} name="facebook"></Icon>
                            <Text style={styles.textIcon}>Anri Dicky's</Text>
                        </View>
                        <View style={styles.rawMargin}>
                            <Icon style={{fontSize:46, color:"#3EC6FF", alignSelf:"center" }} name="instagram"></Icon>
                            <Text style={styles.textIcon}>@dcknr</Text>
                        </View>
                        <View style={styles.rawMargin}>
                        
                        <Icon style={{fontSize:46, color:"#3EC6FF", alignSelf:"center" }} name="twitter"></Icon>
                        <Text style={styles.textIcon}>@dcknr</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }        
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
  
    logoContainer: {
        alignItems: 'center',
    },

    person: {
        marginTop: 20,        
        marginBottom: 20,
        width: 160,
        height: 160
    },

    textPerson:{ 
        fontSize: 22, 
        color: "#003366",
    },

    textDeveloper:{ 
        marginTop: 10,      
        fontSize: 16, 
        color: "#3EC6FF",
    },

    portofolio: {
        marginTop: 40,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        width: WIDTH - 75,
        padding: 10,
    },

    borderHitam: {
        marginTop: 5,
        marginBottom: 5,
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },

    hubungiSaya: {
        marginTop: 10,
        backgroundColor: '#EFEFEF',
        borderRadius: 16,
        width: WIDTH - 75,
        padding: 10,
        
    },

    columnMargin: {
        alignSelf: "center",
        padding: 10,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',

    },

    rawMargin:{
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },

    formInput:{ 
        height: 40, 
        borderColor: "#003366",
        borderWidth: 1,
        marginBottom:20,
        width: WIDTH - 75,
        
    },

    textTitle:{ 
        color: "#003366",
        marginBottom:5,
        fontSize: 18
    },

    textIcon:{ 
        color: "#003366",
        marginBottom:5,
        justifyContent: 'space-between',
        marginLeft: 15,
    },

    formButton: {
        marginTop: 10,
    },

    textButton:{ 
        color: "#FFFFFF",
        fontSize: 18, 
    },

    textInputButton:{ 
        fontSize: 20,
        color: "#3EC6FF",
        marginTop:20,
        marginBottom:20,
        alignSelf: "center"
    },

    buttonMasuk: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius:16,
        width: 100,
        justifyContent: 'center',
    },

    buttonDaftar: {
        alignItems: "center",
        backgroundColor: "#003366",
        padding: 10,
        borderRadius:16,
        width: 100,
        justifyContent: 'center',
    },
});
