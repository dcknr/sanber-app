import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        return (
            
            <TouchableOpacity style={styles.bgSkill}>
                <View style={styles.rowMargin}>
                    <View style={styles.columnMargin}>
                        <Icon style={{fontSize:80, color:"#003366"}} name={skill.iconName}></Icon>
                    </View>
                    <View style={styles.columnMargin}>
                        <Text style={styles.textSkillName}>{skill.skillName}</Text>
                        <Text style={styles.textcategoryName}>{skill.categoryName}</Text>
                        <Text style={styles.textpercentageProgress}>{skill.percentageProgress}</Text>
                    </View>
                    <View style={styles.columnMargin}>
                        <Icon style={{fontSize:80, color:"#003366"}} name="chevron-right"></Icon>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    bgSkill: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        padding: 10,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        elevation: 8,
    },
    textSkillName:{ 
        color: "#003366",
        margin:5,
        fontSize: 24,
        alignSelf: "flex-start"
    },
    textcategoryName:{ 
        color: "#3EC6FF",
        margin:5,
        fontSize: 16,
        alignSelf: "flex-start"
    },
    textpercentageProgress:{ 
        color: "#FFFFFF",
        margin:5,
        fontSize: 48,
        alignSelf: "flex-end"
    },
    columnMargin: {
        alignSelf: "center",
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    rowMargin: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
});
