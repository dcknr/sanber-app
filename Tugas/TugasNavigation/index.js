import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import LoginScreen from '../TugasNavigation/components/LoginScreen';
import AboutScreen from '../TugasNavigation/components/AboutScreen';
import SkillScreen from '../TugasNavigation/components/SkillScreen';
import AddScreen from '../TugasNavigation/components/AddScreen';
import ProjectScreen from '../TugasNavigation/components/ProjectScreen';

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const Drawer = createDrawerNavigator();

const TabsScreen = () => (
    <Tabs.Navigator>
        <Tabs.Screen name="Skill" component={SkillScreen} />
        <Tabs.Screen name="Project" component={ProjectScreen} />
        <Tabs.Screen name="Add" component={AddScreen} />
    </Tabs.Navigator>
);

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="LoginScreen" component={LoginScreen}/>
        <Drawer.Screen name="About" component={AboutScreen} />
        <Drawer.Screen name="Tab Screen" component={TabsScreen}/>
    </Drawer.Navigator>
);

function App() {
    return (
        <NavigationContainer>
            <HomeStack.Navigator headerMode={"none"}>
                <HomeStack.Screen name ="DrawerScreen" 
                component={DrawerScreen} 
                />
            </HomeStack.Navigator>
        </NavigationContainer>
    );
  }
  
export default App;
