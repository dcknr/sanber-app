import React, { Component } from 'react'
import { StyleSheet, Text, Image, View, FlatList, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import data from '../../Tugas14/skillData.json'
import SkillItem from '../../Tugas14/components/SkillItem'

export default class SkillScreen extends Component {
    render(){
        return (
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image
                    style={styles.logo}
                    source = {require("../../Tugas13/images/logo.png")}
                />
            </View>
            <View style={styles.profileContainer}>
                <Icon style={{fontSize:25, color:"#3EC6FF", alignSelf:"center" }} name="account-circle"></Icon>
                <View style={styles.rawMargin}>
                    <Text style={{fontSize:12, marginLeft: 5} }>Hai,</Text>
                    <Text style={{fontSize:16, color: "#003366", alignSelf:"center", marginLeft: 5} }>Anri Dicky Septiawan</Text>
                </View> 
            </View>
            <View style={{marginTop: 30, marginLeft: 20}}>
                <Text style={{fontSize:36, color: "#003366"}}>SKILL</Text>
            </View>
            <View style={styles.border}>
            </View>
            
            <View style={styles.bodySkill}>
                <View style={styles.rowMargin}>
                    <TouchableOpacity style={styles.bgCategory}>
                        <Text style={styles.textcategoryName}>Library / Framework</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bgCategory}>
                        <Text style={styles.textcategoryName}>Bahasa Pemrograman</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.bgCategory}>
                        <Text style={styles.textcategoryName}>Teknologi</Text>
                    </TouchableOpacity>
                </View>
            </View>
                
            <FlatList
            data={data.items}
            renderItem={(skill)=><SkillItem skill={skill.item} />}
            keyExtractor={(skill)=>skill.id}
            />

        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    logoContainer: {
        marginTop: 20,
        flexDirection: 'row', 
        justifyContent: 'flex-end'
    },
    logo:{
        width: 187,
        height:51
    },
    profileContainer: {
        flexDirection: 'row', 
        justifyContent: 'flex-start',
        marginLeft: 20,
        marginTop: 10
    },
    border: {
        marginTop: 5,
        borderBottomColor: "#3EC6FF",
        borderBottomWidth: 5,
        marginLeft: 20,
        marginRight: 20,
    },
    bgCategory: {
        backgroundColor: '#B4E9FF',
        borderRadius: 8,
        padding: 10,
        marginTop: 5,
    },
    rowMargin: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },
    textcategoryName:{ 
        color: "#003366",
        fontSize: 14,
        alignSelf: "flex-start"
    },
    bodySkill:{ 
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 5,
        marginTop: 5,
        marginBottom: 10,
    },
});