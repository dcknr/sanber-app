// import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, TextInput, Image, View, TouchableOpacity, Dimensions } from 'react-native';

const {width : WIDTH} = Dimensions.get('window')

export default class LoginScreen extends Component  {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source = {require("../../Tugas13/images/logo.png")}
                    />
                    <Text style={styles.textLogin}>Login</Text>
                </View>
                
                <View style={styles.form}>
                    <Text style={styles.textInput}>Username / Email</Text>
                    <TextInput
                    style={styles.formInput}
                    />
                    <Text style={styles.textInput}>Password</Text>
                    <TextInput secureTextEntry={true}
                    style={styles.formInput}
                    />
                </View>
        
                <View style={styles.formButton}>
                    {/* Button Masuk */}
                    <TouchableOpacity style={styles.buttonMasuk}>
                        <Text style={styles.textButton}>Masuk</Text>
                    </TouchableOpacity>
        
                    <Text style={styles.textInputButton}>atau</Text>
        
                    {/* Button Daftar */}
                    <TouchableOpacity style={styles.buttonDaftar}>
                        <Text style={styles.textButton}>Daftar?</Text>
                    </TouchableOpacity>
              </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
  
    logoContainer: {
        alignItems: 'center',
    },

    logo: {
        resizeMode: "contain",
    },

    textLogin:{ 
        fontSize: 24, 
        marginTop: 50,
        color: "#003366",
        fontWeight: '200'
    },

    form: {
        marginTop: 40,
    },

    formInput:{ 
        height: 40, 
        borderColor: "#003366",
        borderWidth: 1,
        marginBottom:20,
        width: WIDTH - 75,
        
    },

    textInput:{ 
        color: "#003366",
        marginBottom:5,
    },

    formButton: {
        marginTop: 10,
    },

    textButton:{ 
        color: "#FFFFFF",
        fontSize: 18, 
    },

    textInputButton:{ 
        fontSize: 20,
        color: "#3EC6FF",
        marginTop:20,
        marginBottom:20,
        alignSelf: "center"
    },

    buttonMasuk: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius:16,
        width: 100,
        justifyContent: 'center',
    },

    buttonDaftar: {
        alignItems: "center",
        backgroundColor: "#003366",
        padding: 10,
        borderRadius:16,
        width: 100,
        justifyContent: 'center',
    },
});
